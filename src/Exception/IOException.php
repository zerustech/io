<?php

/**
 * This file is part of the ZerusTech package.
 *
 * (c) Michael Lee <michael.lee@zerustech.com>
 *
 * For full copyright and license information, please view the LICENSE file that
 * was distributed with this source code.
 */
namespace ZerusTech\Component\IO\Exception;

/**
 * Signals that an I/O exception of some sort has occurred.
 *
 * This class is the general class of exceptions produced by failed or
 * interrupted I/O operations.
 *
 * @author Michael Lee <michael.lee@zerustech.com>
 */
class IOException extends \RuntimeException
{
}
