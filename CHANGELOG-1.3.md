CHANGELOG for 1.3.x
=====================

This changelog references the relavant changes (bug and security fixes) done in
1.3 minor versions.

To get the new features in this major release, check the list at the bottom of
this file.

* 1.3.0 (2016-08-13)
    * Added class ``PipedInputStream``.
    * Added class ``PipedOutputStream``.
    * Added code samples
