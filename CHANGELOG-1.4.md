CHANGELOG for 1.4.x
=====================

This changelog references the relavant changes (bug and security fixes) done in
1.4 minor versions.

To get the new features in this major release, check the list at the bottom of
this file.

* 1.4.0 ()
    * Added filter input stream factory classes.
    * Added filter input stream factory resolver classes.
