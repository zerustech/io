CHANGELOG for 1.0.x
=====================

This changelog references the relavant changes (bug and security fixes) done in
1.0 minor versions.

To get the new features in this major release, check the list at the bottom of
this file.

* 1.0.6 (2016-08-17)
    * Deprecated read()
    * Deprecated write()

* 1.0.5
    * Add dev-master alias back

* 1.0.4
    * Removed composer.lock
    * Removed dev-master alias from composer.json
    * Moved all source files to /src
    * Synchronized class changes from 1.1 reversely - this is an one-off
      workaround to fix the version tree.

* 1.0.3
    * Added composer.lock

* 1.0.2
    * Updated branch alias for master
    * Updated travis build icon

* 1.0.1 (2016-07-08)
    * Updated travis build icon

* 1.0.0 (2016-07-08)
    * First release of the zerustech io component.
    * Added class ``AbstractStream``.
    * Added class ``FileInputStream``.
    * Added class ``StringInputStream``.
    * Added class ``FileOutputStream``.
    * Added other basic I/O related classes and interfaces.
